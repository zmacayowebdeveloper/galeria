<?php

namespace App\Helpers\Frontend;
use Mail;
use Illuminate\Support\Facades\View;

class EnviosCorreo {

 public static function sendMailContacto($template, $params_template,  $to_emails, $subject){

    //desarrollo

      // Mail::send($template, ['data' => $params_template], function($message) use ($to_emails, $subject)
      // {
      //    $message->from('info@galeriaisaarte.com', 'GaleríaIsaArte');
      //    $message->to($to_emails)->subject($subject);
      // });

    //produccion
     try {

       $view = View::make($template, [
             'data' => $params_template
       ]);

       $html = $view->render();
       $to=$to_emails;

       $headers = "From: info@galeriaisaarte.com" . "\r\n";
       $headers .= "MIME-Version: 1.0\r\n";
       $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
       // $headers .= "Cc: asesorvirtual@indianmotos.com\r\n";
       // $headers .= "Cc: jmarketing@indianmotos.com\r\n";

       mail($to, "=?UTF-8?B?".base64_encode($subject)."?=", $html, $headers);

       } catch (Exception $e) {
           dd($e);
     }

 }


}
