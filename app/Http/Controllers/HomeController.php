<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Frontend\EnviosCorreo as HelperCorreo;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{

   public function index(){
     $cuadros = \App\Cuadro::all();
     $blogs = \App\Blog::all();

     return view('frontend.inicio.index', compact('cuadros', 'blogs'));
   }

   public function contacto(Request $request)
   {
       $validator = Validator::make($request->all(), [
           'nombre' => 'required',
           'email'     => 'required|email',
           'telefono'  => 'required',
           'consulta' => 'required',

         ]);
         if ($validator->fails()) {
           return response()->json($validator->errors(), 400);
         }

       $contacto= new \App\Contacto;
       $contacto->cuadro_id = $request->input('cuadro_id');
       $contacto->nombre = $request->input('nombre');
       $contacto->email = $request->input('email');
       $contacto->telefono = $request->input('telefono');
       $contacto->consulta = $request->input('consulta');
       $contacto->save();

       $emails = 'isabel@galeriaisaarte.com';
       $subject = 'Nuevo contacto desde la web Galeria Isa Arte: ' ;
       HelperCorreo::sendMailContacto('emails.contacto', $contacto, $emails, $subject);

       return response()->json(array('msg' => "creado"), 201);
     }

    public function galeria($galeria)
    {

       $cuadros = \App\Cuadro::all();

        foreach ($cuadros as $key => $value) {
          if(str_slug($value->titulo, '-') == $galeria){
            $galeria = $value;
          }
        }

        $cuadro = \App\Cuadro::find($galeria->id);
        return view('frontend.galeria', compact('cuadro'));
    }


    public function blog($b)
    {
      $blogs = \App\Blog::all();

       foreach ($blogs as $key => $value) {
         if(str_slug($value->titulo, '-') == $b){
           $b = $value;
         }
       }

       $blog = \App\Blog::find($b->id);
       return view('frontend.blog', compact('blog'));
    }


}
