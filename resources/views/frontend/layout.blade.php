<?php
  $seo_title = 'Galeria Isa Arte, Galeria de arte, GaleriaIsaArte, Cuadros de piedras, un Cuentacuentos con poesia';
  $seo_descripcion = 'Cuadros de Piedras realizados con ideas que llegan a mi fantasía, que exploran mi creatividad, que estimulan mi imaginación. Entonces surge un diálogo íntimo, poderoso, que me guía en el proceso de composición';
 ?>

<!doctype html>

<html lang="es">
<head>
<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title> {{ $seo_title }} </title>
<meta name="description" content="{{ $seo_descripcion }} ">
<link rel="icon" href="{{ asset('images/favicon.ico') }}"  type="image/x-icon"/>
<link rel="stylesheet" href="{{ url('css/app.css?v=1.0') }}">
<link rel="stylesheet" href="{{ url('css/assets/slick.css') }}">
<link rel="stylesheet" href="{{ url('css/assets/slick-theme.css') }}">
<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"
@yield('stylesheet')
</head>
<body>


@yield('content')


@include('frontend.partials.politica')
@include('frontend.partials.footer')
<script src="{{ url('js/app.js') }}"></script>
<script type="text/javascript">

  const site_url = '{{ url('/') }}/';
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
<script src="{{ url('js/assets/slick.min.js') }}"></script>
<script src="{{ url('js/assets/sweetalert.min.js') }}"></script>
<script src="{{ url('js/assets/jquery.validate.min.js') }}"></script>
<script src="{{ url('js/assets/contacto.js') }}"></script>
<script>
$(document).ready(function() {
  if (screen.width < 1024) {
    $('.blog-boxes-slick').slick({
          infinite: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          dots: false,
          autoplay: true,
          autoplaySpeed: 2000,
          arrows: false,
          responsive: [{
              breakpoint: 950,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1
              }
          },
          {
              breakpoint: 760,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          }

          ]

      });
  }else{
    $('.blog-boxes-slick').slick({
          infinite: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          dots: false,
          autoplay: true,
          autoplaySpeed: 2000,
          arrows: true,
          responsive: [{
              breakpoint: 950,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1
              }
          },
          {
              breakpoint: 760,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          }
          ]

      });
  }


  $('a[href^="#"]').click(function() {
    var destino = $(this.hash);

    if (destino.length == 1) {
      destino = $('#' + this.hash.substr(1));

    }
    $('html, body').animate({ scrollTop: destino.offset().top }, 800);
    return false;
  });
});
</script>
@yield('scripts')
</body>
</html>
