@extends('frontend.layout')
@section('content')
<?php
  if($cuadro->estado == 1){
    $estado = 'Disponible';
  }else{
    $estado = 'Vendido';
  }

  $seo_title = $cuadro->titulo;
  $seo_descripcion = 'Cuadros de Piedras realizados con ideas que llegan a mi fantasía, que exploran mi creatividad, que estimulan mi imaginación. Entonces surge un diálogo íntimo, poderoso, que me guía en el proceso de composición';

?>

<section class="section sm section-blog2 mt-6">
    <div class="container">
        <div class="content-post">
          <p class=" color-primary "><strong>{{ $cuadro->titulo }}</strong></p>
          <p class=" color-primary ">{{ $cuadro->precio }} {{ $estado }}</p>

            <img class="img-post" src="{{ url('storage/'.$cuadro->imagen) }}" alt="">

            <p>{{ $cuadro->descripcion }}</p>
        </div>
        <!-- =============================== leave coment -->
        <form id="formContacto" action="{{ url('contacto') }}" method="post">
          <input type="hidden" name="_token" id="csrf_toKen" value="{{ csrf_token() }}">
          <input type="hidden" name="cuadro_id" value="{{ $cuadro->id }}">

          <div class="container mb-6">
              <div class="coments-wrap">
                  <h3 class="coments-wrap-title">Consultame</h3>
                  <div class="row">
                      <div class="col-md-6">
                          <label> Tu consulta </label>
                          <textarea class="form-control" name="consulta" placeholder="Escribeme tu consulta" required></textarea>
                      </div>
                      <div class="col-md-6">
                          <label> Tu Nombre </label>
                          <input type="text" class="form-control" name="nombre" placeholder="Tu nombre" required>
                      </div>
                      <div class="col-md-6">
                          <label> Tu Email </label>
                          <input type="email" class="form-control" name="email" placeholder="Tu email">
                      </div>
                      <div class="col-md-6">
                          <label> Tu Teléfono </label>
                          <input type="text" class="form-control" name="telefono" placeholder="Tu teléfono">
                      </div>
                      <div class="col-md-12">
                          <input type="checkbox" id"terminos" name="terminos" required>
                          Al hacer clic en "Enviar" certifico que acepto la
                          <a href="#" data-toggle="modal" data-target="#modalPolitica" id="politica" style="text-decoration:none; color:#000;">
                            <strong>Política de privacidad del sitio.</strong>
                          </a>
                      </div>
                      <div class="col-md-6">
                        <button type="submit" class="btn btn-primary btn-enviar">Enviar</button>
                      </div>

                  </div>
              </div>
          </div>
        </form>

    </div>
</section>

<div class="toTop2">
 <a href="{{ url('/') }}" id="toTop2" style="display:block">
   <span>Regresar</span>
 </a>
</div>

@endsection

@section('scripts')
<script type="text/javascript">

</script>

@endsection
