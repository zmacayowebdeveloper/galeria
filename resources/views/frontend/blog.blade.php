@extends('frontend.layout')
@section('content')

<?php
  $seo_title = $blog->titulo;
  $seo_descripcion = 'Cuadros de Piedras realizados con ideas que llegan a mi fantasía, que exploran mi creatividad, que estimulan mi imaginación. Entonces surge un diálogo íntimo, poderoso, que me guía en el proceso de composición';
 ?>

<section class="section sm section-blog2 mt-6">
      <div class="container">
          <div class="content-post">
              <p class=" color-primary "><strong>{{ $blog->titulo }}</strong></p>
              <img class="img-post" src="{{ url('storage/'.$blog->imagen) }}" alt="">
              <p>
                {!! $blog->descripcion !!}
              </p>
            <strong style="font-size: 18px;"><span class="post-author">Por <a rel="author">{{ $blog->autor }}</a></span>  <span class="post-date"> {{ date("d", strtotime($blog->fecha))  }} {{ obtenerNombreMes($blog->fecha)  }} </span></strong>
          </div>
      </div>
  </section>

  <div class="toTop2">
   <a href="{{ url('/') }}" id="toTop2" style="display:block">
     <span>Regresar</span>
   </a>
  </div>

@endsection
<?php
function obtenerNombreMes($fecha){
      $anno = date("Y", strtotime($fecha));
        $mes = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
        $mes = $mes[(date('m', strtotime($fecha))*1)-1];
        return $mes.', '.$anno;
  }

?>
@section('scripts')
<script type="text/javascript">

</script>

@endsection
