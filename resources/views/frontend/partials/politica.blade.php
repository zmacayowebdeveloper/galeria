<div class="modal fade modalPolitica" id="modalPolitica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close cerrar" data-dismiss="modal" aria-label="Close">
          <img src="{{ url('/') }}/images/cerrarVerde.png" class="img-fluid" alt="">
        </button>
        <div class="container">
          <div class="row">
            <div class="col-md-10 m-auto">
              <div class="modalPolitica__content">
                  <div class="text-center">
                    <h2 class="titulo titulo--mediano titulo--celeste pb-3">POLÍTICA DE PRIVACIDAD</h2>
                  </div>
                  <p>
                    A través de este sitio web no se recaban datos de carácter personal de los usuarios sin su conocimiento, ni se ceden a terceros.
                  </p>
                  <p>
                    Con la finalidad de ofrecerle el mejor servicio y con el objeto de facilitar el uso, se analizan el número de páginas visitadas, el número de visitas, así como la actividad de los visitantes y su frecuencia de utilización. A estos efectos, https://galeriaisaarte.com utiliza la información estadística elaborada por el Proveedor de Servicios de Internet.
                  </p>
                  <p>
                    GaleríaISAARTE.com no utiliza cookies para recoger información de los usuarios, ni registra las direcciones IP de acceso. Únicamente se utilizan cookies propias,  de sesión, con finalidad técnica (aquellas que permiten al usuario la navegación a través del sitio web y la utilización de las diferentes opciones y servicios que en ella existen).
                  </p>
                  <p>
                    El portal del que es titular GaleríaISAARTE.com no contiene enlaces a sitios web de terceros.
                  </p>
                  <p>
                    <strong>Información básica sobre protección de datos</strong>
                  </p>
                  <p>
                    A continuación le informamos sobre la política de protección de datos de GaleríaISAARTE.com
                  </p>
                  <p>
                    <strong>Responsable del tratamiento</strong>
                  </p>
                  <p>
                    Los datos de carácter personal que se pudieran recabar directamente del interesado serán tratados de forma confidencial y quedarán incorporados a la correspondiente actividad de tratamiento titularidad de  GaleríaISAARTE.com
                  </p>
                  <p>
                    <strong>Finalidad</strong>
                  </p>
                  <p>
                    La finalidad del tratamiento de los datos corresponde a cada una de las actividades de tratamiento que realiza GaleríaISAARTE.com
                  </p>
                  <p>
                    <strong>Legitimación</strong>
                  </p>
                  <p>
                    El tratamiento de sus datos se realiza cuando la finalidad del tratamiento requiera su consentimiento, que habrá de ser prestado mediante una clara acción afirmativa.
                  </p>
                  <p>
                    <strong>Conservación de datos</strong>
                  </p>
                  <p>
                    Los datos personales proporcionados se conservarán durante el tiempo necesario para cumplir con la finalidad para la que se recaban y para determinar las posibles responsabilidades que se pudieran derivar de la finalidad, además de los períodos establecidos en la normativa de archivos y documentación.
                  </p>
                  <p>
                    <strong>Comunicación de datos</strong>
                  </p>
                  <p>
                    Con carácter general no se comunicarán los datos personales a terceros, salvo obligación legal, entre las que pueden estar las comunicaciones al Defensor del Pueblo, Jueces y Tribunales,.
                  </p>
                  <p>
                    <strong>Derechos de los interesados</strong>
                  </p>
                  <p>
                    Cualquier persona tiene derecho a obtener confirmación sobre los tratamientos que de sus datos que se llevan a cabo por GaleríaISAARTE.com
                  </p>
                  <p>
                    Puede ejercer sus derechos de acceso, rectificación, supresión y portabilidad de sus datos, de limitación y oposición a su tratamiento,  cuando procedan en la dirección de correo electrónico <a href="mailto:isabel@galeriaisaarte.com" >isabel@galeriaisaarte.com</a>
                  </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
