<footer class="footer">
    <div class="footer__container justify-content-center">
      <div class="">
        <div class="col-md-12">
            <div class="">
              <div class="col-md-12">
                 <p class="footer__item">© 2020 GaleriaIsaArte. Todos los derechos reservados by <a href="https://www.facebook.com/Desarrollo-de-Software-zmacayo-110383650678520" target="_blank">
                 softwaredeveloperzm</a> </p>
                 <p class="footer__item">
                     <a href="#" data-toggle="modal" data-target="#modalPolitica" id="politica">
                       Política de privacidad.
                     </a>
                 </p>
              </div>
            </div>
        </div>
      </div>
    </div>
</footer>
