<header class="header" id="menu">
  <div class="header__bottom" style="background-image:url({{ url('images/fondo_piedras2.jpg') }});" id="inicio">
    <div class="">
      <div class="box">
        <div class="">
          <div class="">
            <nav class="navegador">
              <ul class="navegador__link">
                <li>
                  <a href="#galeria" id="miofrenda" class="links">Galería</a>
                </li>
                <li>
                  <a href="#acercademi" id="home" class="links">Acerca de Mí</a>
                </li>
                <li>
                  <a href="#blog" id="nosotros" class="links">Blog</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="">
          <div class="col-md-8 offset-md-2 logo" style="background-image:url({{ url('strg/logo_transparente-min.png') }});">
          </div>

        </div>
      </div>
    </div>
  </div>
</header>
