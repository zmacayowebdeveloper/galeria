@extends('frontend.layout')

@include('frontend.partials.header')

@section('content')

<section  class="galeria" >
    <div class="galeria__content" >
      <div class="">
        <div class="col-md-12 col-12 ustify-content-center">
          <div class="galeria__content__titulo" id="galeria">
            <h1 class="galeria__content__titulo--primero" >Galería</h1>
            <p class="galeria__content__titulo--descripcion">Una creación propia artesanal expresada a través de las piedras, llena de magia y fantasía.</p>
          </div>
        </div>
      </div>
        <div class="container section-blog">
            <div class="work-boxes blog-boxes-slick">
                @foreach($cuadros as $cuadro)
                <?php
                  $slug = str_slug($cuadro->titulo, '-');
                  if($cuadro->estado == 1){
                    $estado = 'Disponible';
                  }else{
                    $estado = 'Vendido';
                  }
                ?>
                  <div class="">
                      <div class="post-wrap mt-0">
                          <a href="{{ url('galeria/'.$slug) }}">
                              <div class="post-img">
                                  <img src="{{ url('storage/'.$cuadro->imagen) }}" alt="">
                              </div>
                          </a>
                          <div class="post-content mb-0">
                              <div class="post-meta">
                                @if($cuadro->estado == 1)
                                  <div class="post-tag"> <span class="tag-item"><strong>{{ $cuadro->precio }}</strong> {{ $estado }}</span> </div>
                                @else
                                  <div class="post-tag"> <span class="tag-item2"><strong>{{ $cuadro->precio  }}</strong>  {{ $estado }}</span> </div>
                                @endif
                              </div>
                              <h3 class="post-title"><a href="{{ url('galeria/'.$slug) }}">{{ $cuadro->titulo }}
                                  </a>
                              </h3>
                          </div>
                      </div>
                  </div>
                @endforeach
            </div>
        </div>

      <div class="col-md-12 col-12 ustify-content-center">
        <div class="galeria__content__titulo" id="galeria">
          <p class="galeria__content__titulo--descripcion galeria-description">Mis cuadros se presentan en las siguientes dimensiones: <strong> Standar 24 x 30 cm y grande 30 x 40 cm. </strong></p>
        </div>
      </div>
    </div>
</section>
<section  class="acercademi" >
  <div class="acercademi__content--isabel" >
    <div class="">
      <div class="col-md-12 col-12 justify-content-center">
        <div class="acercademi__content__titulo" id="acercademi">
          <h1 class="acercademi__content__titulo--primero">Acerca de Mí</h1>
          <h2 class="acercademi__content__titulo--segundo">Isabel</h2>
          <p class="galeria__content__titulo--descripcion">Me llamo Isabel, y vivo en la isla de la Palma, Canarias. Y realizo cuadros hechos con piedras, que recojo de la playa, en el Puerto de Tazacorte. Son una creación artística de escultura, que relatan pequeñas historias llenas de magia y fantasía.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="testimonio">
    <div class="container">
      <div class=" testimonio-box">
        <div class="box-left">
          <img src="{{ url('images/isabel.jpg') }}" alt="">
        </div>
        <div class="box-right">
           <p class="testimonial-quote" id="nosotros">
             "Cuando veo a una piedra, ella me llama. Es una sensación espontánea, grata, y siempre, sorprendente. Surgen ideas que exploran mi creatividad, que estimulan mi imaginación.
             Entonces surge un diálogo íntimo, que me guía en el proceso de composición."
           </p>
           <p class="galeria__content__descripcion">Son obras realizadas en relieve, en 3D, montados en un marco sin cristal.</p>
           <p class="galeria__content__descripcion">Pequeños, adaptables, y muy decorativos.</p>
            <a href="#galeria" class="btn btn-white btn-outline "> Ver mi galería</a>
        </div>
      </div>
    </div>
  </div>
</section>

<section  class="blog">
    <div class="blog__content">
      <div class="">
        <div class="col-md-12 col-12 justify-content-center">
          <div class="blog__content__titulo" id="blog">
            <h1 class="blog__content__titulo--primero">Blog</h1>
          </div>
        </div>
      </div>
      <div class="container section-blog-b">
        <div class="work-boxes blog-boxes-slick">
          @foreach($blogs as $blog)
          <?php
            $slug = str_slug($blog->titulo, '-');
          ?>
          <div class="">
              <div class="post-wrap mt-0">
                  <a href="{{ url('blog/'.$slug) }}">
                      <div class="post-img">
                          <img src="{{ url('storage/'.$blog->imagen) }}" alt="">
                      </div>
                  </a>
                  <div class="post-content mb-0">
                      <h3 class="post-title"><a href="{{ url('blog/'.$slug) }}">{{ $blog->titulo_corto }}
                          </a>
                      </h3>

                      <span class="post-author">Por <a rel="author">{{ $blog->autor }}</a></span>
                      <span class="post-date"> {{ date("d", strtotime($blog->fecha))  }} {{ obtenerNombreMes($blog->fecha)  }} </span>
                  </div>
              </div>
          </div>

          @endforeach
        </div>
    </div>
    </div>
</section>


<div class="toTop">
 <a href="#inicio" id="toTop" style="display:block">
   <ion-icon class="icon-top" name="arrow-up"></ion-icon>
 </a>
</div>

<?php
function obtenerNombreMes($fecha){
      $anno = date("Y", strtotime($fecha));
        $mes = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
        $mes = $mes[(date('m', strtotime($fecha))*1)-1];
        return $mes.', '.$anno;
  }

?>

@endsection

@section('scripts')
<script type="text/javascript">
  $('.toTop').css('display', 'none');

  $(window).scroll( function() {

   var windowHeight = $(window).scrollTop();
   var inicio= $(".galeria").offset();

   inicio = inicio.top;

   if( windowHeight < inicio - 150 ){
     $('.toTop').css('display', 'none');
   }else{
     $('.toTop').css('display', 'block');
   }

  });
</script>

@endsection
