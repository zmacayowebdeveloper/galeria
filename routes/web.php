<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('galeria', function () {
    return redirect('/');
});

Route::get('blog', function () {
    return redirect('/');
});

Route::get('/', 'HomeController@index');
Route::get('galeria/{galeria}', 'HomeController@galeria');
Route::get('blog/{blog}', 'HomeController@blog');
Route::post('contacto', 'HomeController@contacto');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
