$("#formContacto").validate({
  rules: {
      nombre: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
     telefono: {
        required: true
      },
      consulta: {
         required: true
       },
       terminos: {
          required: true
        },

    }
    ,errorPlacement: function(error, element) {
    error.insertBefore( element );
    },

    success: function(label, element) {
        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        $(element).addClass('is-invalid');
          if($(".error").hasClass("is-invalid")){
            $(".labelerror").css("display", "block");
          }
    },
    invalidHandler: function(form, validator) {
        validator.focusInvalid();
    },
    submitHandler: function (form) {
        var token = $("input[name=_token]").val();
        var formData = new FormData(form);
        var url = $(form).attr('action');
        swal("Enviando...", {
          button: false,
        });
        postData(token, formData, url);
    }
});

function postData(token, formData, url){
  $.ajax({
    url: url,
    method: 'POST',
    data: formData,
    dataType: 'json',
    cache: false,
    contentType: false,
    processData: false,
    headers: {
        'X-CSRF-TOKEN': token.content
    },
    success: function (response) {
      alerta();
      clearForm();
    }
  });
}

function alerta(){
    swal(
      '¡Enviado con éxito!',
      'Pronto nos comunicaremos contigo',
      'success'
    )
}

function clearForm() {

	$(':input').each(function() {
	  var type = this.type;
	  var tag = this.tagName.toLowerCase();
		var filename = $("#chooseFile").val();

	  if (type == 'text' || type == 'password' || tag == 'textarea' || type == 'email' || type == 'file')
	    this.value = "";

	  else if (type == 'checkbox' || type == 'radio')
	    this.checked = false;

	  else if (tag == 'select')
	    this.selectedIndex = "";

	});

	clearFormStyles();
}

function clearFormStyles () {
	$('.has-error').each(function (value, index) {
		$(this).removeClass('has-error');
	});

	$('.msgContacto').each(function () {
		$(this).html('');
	});

	$('.msgFreepas').each(function () {
		$(this).html('');
	});
}
