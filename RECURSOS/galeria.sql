-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 26-06-2020 a las 12:18:31
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `galeria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo_corto` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titulo` text COLLATE utf8mb4_unicode_ci,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autor` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `blogs`
--

INSERT INTO `blogs` (`id`, `titulo_corto`, `titulo`, `descripcion`, `imagen`, `autor`, `fecha`, `created_at`, `updated_at`) VALUES
(1, 'Título: “Una cara de piedra”', 'Un potencial de aprendizaje con piedras y una lista de recomendaciones', '<p style=\"box-sizing: border-box; margin: 0px 0px 1.3em; padding: 0px; border: 0px; font-size: 22.5px; line-height: 36px; font-family: \'Crimson Text\', \'Times New Roman\', Times, serif; -webkit-font-smoothing: antialiased;\">Cuando compongo mis cuadros hechos con piedras, se suceden muchos motivos. Entre muchos, me gustan las piedras, expreso emociones, me encanta el arte, y soy una artista que vende cuadros de piedras.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1.3em; padding: 0px; border: 0px; font-size: 22.5px; line-height: 36px; font-family: \'Crimson Text\', \'Times New Roman\', Times, serif; -webkit-font-smoothing: antialiased;\">Tambi&eacute;n hay otros recomendados motivos; he aqu&iacute; una lista de algunos de ellos:</p>\r\n<ul style=\"box-sizing: border-box; margin: 0px 0px 1.3em 2em; padding: 0px; border: 0px; font-size: 22.5px; line-height: 36px; font-family: \'Crimson Text\', \'Times New Roman\', Times, serif; list-style-position: initial; list-style-image: initial; -webkit-font-smoothing: antialiased;\">\r\n<li style=\"box-sizing: border-box; margin: 0px 0px 0.75em; padding: 0px; border: 0px; line-height: 36px; font-family: inherit; text-align: left; -webkit-font-smoothing: antialiased;\">primero, ayuda a mantener y desarrollar nuestra psicomotricidad fina; al manejar peque&ntilde;as o grandes piedras potenciamos la movilidad de nuestros dedos y de las manos. Se convierte en un ejercicio sano y repercute en una mejor salud,</li>\r\n<li style=\"box-sizing: border-box; margin: 0px 0px 0.75em; padding: 0px; border: 0px; line-height: 36px; font-family: inherit; text-align: left; -webkit-font-smoothing: antialiased;\">segundo, fomenta nuestra curiosidad, intentando imaginar/identificar/explorar otras formas de crear y fantasear; explorando distintas maneras con materiales de la propia naturaleza.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px 0px 0.75em; padding: 0px; border: 0px; line-height: 36px; font-family: inherit; text-align: left; -webkit-font-smoothing: antialiased;\">tercero, estimula nuestra capacidad de abstracci&oacute;n y rotaci&oacute;n; pues una piedra puede tener mil representaciones y al rotarlas, ver m&aacute;s posibilidades de expresi&oacute;n, formas.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px 0px 0.75em; padding: 0px; border: 0px; line-height: 36px; font-family: inherit; text-align: left; -webkit-font-smoothing: antialiased;\">cuarto, es una forma de socializar nuestras emociones/sentimientos, dando rienda suelta a un proceso creativo propio, motivador y compartido.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px 0px 0.75em; padding: 0px; border: 0px; line-height: 36px; font-family: inherit; text-align: left; -webkit-font-smoothing: antialiased;\">quinto, potencia el ingenio para encontrar y usar materiales naturales, reciclados con los que podemos construir con nuestra imaginaci&oacute;n.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px 0px 0.75em; padding: 0px; border: 0px; line-height: 36px; font-family: inherit; text-align: left; -webkit-font-smoothing: antialiased;\">sexto, desarrolla nuestra creatividad, para apreciar la sencillez de la belleza de la naturaleza, y componer inspir&aacute;ndonos en ella.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px 0px 0.75em; padding: 0px; border: 0px; line-height: 36px; font-family: inherit; text-align: left; -webkit-font-smoothing: antialiased;\">s&eacute;ptimo, entrena nuestras capacidades cognitivas, mejorando nuestra atenci&oacute;n, concentraci&oacute;n y nuestras funciones ejecutivas.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px 0px 0.75em; padding: 0px; border: 0px; line-height: 36px; font-family: inherit; text-align: left; -webkit-font-smoothing: antialiased;\">octava, promueve a aprender de los errores; a explorar t&aacute;cticas de ajuste, de pegado, de composiciones, de combinaciones de colores, de marcos; de ir depurando y mejorando nuestra t&eacute;cnica y destrezas.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px 0px 0.75em; padding: 0px; border: 0px; line-height: 36px; font-family: inherit; text-align: left; -webkit-font-smoothing: antialiased;\">novena, nos proporciona un tiempo y un espacio de relajaci&oacute;n, de concentrarnos en nosotros mismos.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; line-height: 36px; font-family: inherit; text-align: left; -webkit-font-smoothing: antialiased;\">d&eacute;cima, es divertido; una creaci&oacute;n art&iacute;stica desde nuestro interior; una visi&oacute;n del mismo mundo compartido.</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1.3em; padding: 0px; border: 0px; font-size: 22.5px; line-height: 36px; font-family: \'Crimson Text\', \'Times New Roman\', Times, serif; -webkit-font-smoothing: antialiased;\">Y podr&iacute;a seguir a&ntilde;adiendo m&aacute;s. Lo cierto. es que quiero vender mi arte pero tambi&eacute;n fomentar que a trav&eacute;s del arte podemos desarrollar un potencial de aprendizaje.</p>', 'blog\\June2020\\I8woMAOoSRlPr5dMRQvs.jpg', 'Isabel', '2020-06-21', '2020-06-22 16:24:43', '2020-06-22 16:27:18'),
(2, 'Título: “Pintando en las nubes”', 'Un tragaluz y un marco', '<p style=\"box-sizing: border-box; margin: 0px 0px 1.3em; padding: 0px; border: 0px; font-size: 22.5px; line-height: 36px; font-family: \'Crimson Text\', \'Times New Roman\', Times, serif; -webkit-font-smoothing: antialiased;\">A trav&eacute;s del tragaluz, escucho el sonido de la calle, veo los reflejos del sol, oigo el canto de alg&uacute;n canario en las nubes, siento el soplido de la brisa en las flores. Aprecio el reflejo de la luz en los espejos. Imagino.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1.3em; padding: 0px; border: 0px; font-size: 22.5px; line-height: 36px; font-family: \'Crimson Text\', \'Times New Roman\', Times, serif; -webkit-font-smoothing: antialiased;\">Hoy me han devuelto un cuadro que ten&iacute;a en un bar-exposici&oacute;n que se ha ca&iacute;do al suelo, y ha sido como una se&ntilde;al iluminada. Tan solo se le cay&oacute; una piedra, aunque se rompi&oacute; el marco. Fue una posibilidad para transformarlo. Y con un nuevo marco en blanco, apareci&oacute; la luz.</p>', 'blog\\June2020\\mQ88a6TbueuSntkVc2lh.jpg', 'Isabel', '2020-06-08', '2020-06-22 16:28:15', '2020-06-22 16:28:44'),
(3, 'Título: \"El resplandor del sol\"', 'El resplandor del sol', '<p><span style=\"font-family: \'Crimson Text\', \'Times New Roman\', Times, serif; font-size: 22.5px;\">El resplandor del sol al atardecer. Unos aromas de rayos c&oacute;smicos han resplandecido. Y la vigilante de la gran monta&ntilde;a esta agradecida. Y, yo, volver&eacute; a la luz de los colores, a brillar como ser resplandeciente, en armon&iacute;a con la vital naturaleza que me envuelve.</span></p>', 'blog\\June2020\\tpHg9hjazayrbrudFeO6.jpg', 'Isabel', '2020-05-30', '2020-06-22 16:29:23', '2020-06-23 15:33:44'),
(4, 'Título: \"Mi amigo en el cielo\"', 'Mi amigo en el cielo', '<p style=\"box-sizing: border-box; margin: 0px 0px 1.3em; padding: 0px; border: 0px; font-size: 22.5px; line-height: 36px; font-family: \'Crimson Text\', \'Times New Roman\', Times, serif; -webkit-font-smoothing: antialiased;\">El hombre del desierto se ha ido a navegar por los mares del cielo.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1.3em; padding: 0px; border: 0px; font-size: 22.5px; line-height: 36px; font-family: \'Crimson Text\', \'Times New Roman\', Times, serif; -webkit-font-smoothing: antialiased;\">&iexcl;Le echo de menos!</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1.3em; padding: 0px; border: 0px; font-size: 22.5px; line-height: 36px; font-family: \'Crimson Text\', \'Times New Roman\', Times, serif; -webkit-font-smoothing: antialiased;\">&Eacute;l sigue acompa&ntilde;&aacute;ndome con su gran esp&iacute;ritu de guerrero, so&ntilde;&aacute;ndole en otros sue&ntilde;os llenos de multitud de luces parpadeantes de este vasto universo.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1.3em; padding: 0px; border: 0px; font-size: 22.5px; line-height: 36px; font-family: \'Crimson Text\', \'Times New Roman\', Times, serif; -webkit-font-smoothing: antialiased;\">&iexcl;Le echo tanto de menos!</p>', 'blog\\June2020\\BXBJ6vr6vvoMFmWx5G1Q.jpg', 'Isabel', '2020-05-08', '2020-06-22 16:30:15', '2020-06-23 15:34:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consulta` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cuadro_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuadros`
--

CREATE TABLE `cuadros` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cuadros`
--

INSERT INTO `cuadros` (`id`, `titulo`, `descripcion`, `imagen`, `precio`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Lloviendo en colores', 'After all, a “comparative advantage” begets the question, compared to what? We export goods to other nations when we can do that relatively better than they can and likewise import goods or services that we have a comparative disadvantage in producing.', 'cuadros\\June2020\\KaYifiwxkUwy9LVJoJxM.jpg', '45 € -', 1, '2020-06-22 12:42:22', '2020-06-22 15:34:32'),
(2, 'En el camino', 'After all, a “comparative advantage” begets the question, compared to what? We export goods to other nations when we can do that relatively better than they can and likewise import goods or services that we have a comparative disadvantage in producing.', 'cuadros\\June2020\\nR1QJW1sULalB5jeD4BR.jpg', '55 € -', 1, '2020-06-22 12:44:31', '2020-06-22 15:34:54'),
(3, 'El árbol caminante', 'After all, a “comparative advantage” begets the question, compared to what? We export goods to other nations when we can do that relatively better than they can and likewise import goods or services that we have a comparative disadvantage in producing.', 'cuadros\\June2020\\P6maumC3xE2mzH1VmOM2.jpg', NULL, 1, '2020-06-22 12:45:40', '2020-06-22 16:16:06'),
(4, 'En la Playa', 'After all, a “comparative advantage” begets the question, compared to what? We export goods to other nations when we can do that relatively better than they can and likewise import goods or services that we have a comparative disadvantage in producing.', 'cuadros\\June2020\\VX32cJkfFdvbMLa4oB3c.jpg', '70 € -', 1, '2020-06-22 12:46:10', '2020-06-22 15:35:42'),
(5, 'Sentidos', 'After all, a “comparative advantage” begets the question, compared to what? We export goods to other nations when we can do that relatively better than they can and likewise import goods or services that we have a comparative disadvantage in producing.', 'cuadros\\June2020\\WkXNr61E9kEMBIVUKGkS.jpg', '60 € -', 1, '2020-06-22 12:47:32', '2020-06-22 15:35:54'),
(6, 'Un día de viento y lluvia', 'After all, a “comparative advantage” begets the question, compared to what? We export goods to other nations when we can do that relatively better than they can and likewise import goods or services that we have a comparative disadvantage in producing.', 'cuadros\\June2020\\S99rGXNQLyQVOwZVWNrY.jpg', NULL, 1, '2020-06-22 12:48:56', '2020-06-22 15:36:05'),
(7, 'Entre Libros', 'After all, a “comparative advantage” begets the question, compared to what? We export goods to other nations when we can do that relatively better than they can and likewise import goods or services that we have a comparative disadvantage in producing.', 'cuadros\\June2020\\8NbnNHj972e43YyNKWZq.jpg', '70 € -', 1, '2020-06-22 12:49:28', '2020-06-22 16:16:29'),
(8, 'La Obra', 'After all, a “comparative advantage” begets the question, compared to what? We export goods to other nations when we can do that relatively better than they can and likewise import goods or services that we have a comparative disadvantage in producing.', 'cuadros\\June2020\\hkw8lNaF20pf7gzkxFVn.jpg', NULL, 0, '2020-06-22 12:50:20', '2020-06-22 15:36:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, NULL, 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(23, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(24, 4, 'titulo', 'text', 'Título', 0, 1, 1, 1, 1, 1, '{}', 2),
(25, 4, 'descripcion', 'text_area', 'Descripción', 0, 0, 1, 1, 1, 1, '{}', 3),
(26, 4, 'imagen', 'image', 'Imagen 1000 x 1266', 0, 1, 1, 1, 1, 1, 'null', 4),
(27, 4, 'precio', 'text', 'Precio', 0, 1, 1, 1, 1, 1, '{}', 5),
(28, 4, 'estado', 'select_dropdown', 'Estado', 1, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"Vendido\",\"1\":\"Disponible\"}}', 6),
(29, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 7),
(30, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(31, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(32, 5, 'titulo_corto', 'text', 'Título Corto', 0, 1, 1, 1, 1, 1, '{}', 2),
(33, 5, 'titulo', 'text', 'Título', 0, 1, 1, 1, 1, 1, '{}', 3),
(34, 5, 'descripcion', 'rich_text_box', 'Descripción', 0, 0, 1, 1, 1, 1, '{}', 4),
(35, 5, 'imagen', 'image', 'Imagen 750 x 422', 0, 1, 1, 1, 1, 1, '{}', 5),
(36, 5, 'autor', 'text', 'Autor', 0, 1, 1, 1, 1, 1, '{}', 6),
(37, 5, 'created_at', 'timestamp', 'Fecha de registro', 0, 0, 0, 0, 0, 0, '{}', 7),
(38, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(39, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(40, 6, 'nombre', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 3),
(41, 6, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 4),
(42, 6, 'telefono', 'text', 'Teléfono', 1, 1, 1, 1, 1, 1, '{}', 5),
(43, 6, 'consulta', 'text', 'Consulta', 1, 1, 1, 1, 1, 1, '{}', 6),
(44, 6, 'cuadro_id', 'text', 'Cuadro Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(45, 6, 'created_at', 'timestamp', 'Fecha de registro', 0, 0, 1, 0, 0, 0, '{}', 7),
(46, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(47, 6, 'contacto_belongsto_cuadro_relationship', 'relationship', 'Cuadro', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Cuadro\",\"table\":\"cuadros\",\"type\":\"belongsTo\",\"column\":\"cuadro_id\",\"key\":\"id\",\"label\":\"titulo\",\"pivot_table\":\"blogs\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(48, 5, 'fecha', 'date', 'Fecha', 0, 1, 1, 1, 1, 1, '{}', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2020-06-22 10:02:17', '2020-06-22 10:02:17'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-06-22 10:02:17', '2020-06-22 10:02:17'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2020-06-22 10:02:17', '2020-06-22 10:02:17'),
(4, 'cuadros', 'cuadros', 'Cuadro', 'Cuadros', NULL, 'App\\Cuadro', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-06-22 10:10:01', '2020-06-22 20:31:00'),
(5, 'blogs', 'blog', 'Blog', 'Blog', NULL, 'App\\Blog', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-06-22 10:11:32', '2020-06-22 16:51:00'),
(6, 'contactos', 'contactos', 'Contacto', 'Contactos', NULL, 'App\\Contacto', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-06-22 10:12:18', '2020-06-22 10:14:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-06-22 10:02:18', '2020-06-22 10:02:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-06-22 10:02:18', '2020-06-22 10:02:18', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-06-22 10:02:18', '2020-06-22 10:02:18', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-06-22 10:02:18', '2020-06-22 10:02:18', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-06-22 10:02:18', '2020-06-22 10:02:18', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-06-22 10:02:18', '2020-06-22 10:02:18', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-06-22 10:02:18', '2020-06-22 10:02:18', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-06-22 10:02:18', '2020-06-22 10:02:18', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-06-22 10:02:19', '2020-06-22 10:02:19', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-06-22 10:02:19', '2020-06-22 10:02:19', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-06-22 10:02:19', '2020-06-22 10:02:19', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-06-22 10:02:21', '2020-06-22 10:02:21', 'voyager.hooks', NULL),
(12, 1, 'Cuadros', '', '_self', NULL, NULL, NULL, 15, '2020-06-22 10:10:01', '2020-06-22 10:10:01', 'voyager.cuadros.index', NULL),
(13, 1, 'Blog', '', '_self', NULL, '#000000', NULL, 16, '2020-06-22 10:11:32', '2020-06-22 10:13:10', 'voyager.blog.index', 'null'),
(14, 1, 'Contactos', '', '_self', NULL, NULL, NULL, 17, '2020-06-22 10:12:18', '2020-06-22 10:12:18', 'voyager.contactos.index', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_06_22_103249_create_cuadros_table', 1),
(4, '2020_06_22_103349_create_blogs_table', 1),
(5, '2020_06_22_103418_create_contactos_table', 1),
(6, '2016_01_01_000000_add_voyager_user_fields', 2),
(7, '2016_01_01_000000_create_data_types_table', 2),
(8, '2016_05_19_173453_create_menu_table', 2),
(9, '2016_10_21_190000_create_roles_table', 2),
(10, '2016_10_21_190000_create_settings_table', 2),
(11, '2016_11_30_135954_create_permission_table', 2),
(12, '2016_11_30_141208_create_permission_role_table', 2),
(13, '2016_12_26_201236_data_types__add__server_side', 2),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 2),
(15, '2017_01_14_005015_create_translations_table', 2),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 2),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 2),
(18, '2017_04_21_000000_add_order_to_data_rows_table', 2),
(19, '2017_07_05_210000_add_policyname_to_data_types_table', 2),
(20, '2017_08_05_000000_add_group_to_settings_table', 2),
(21, '2017_11_26_013050_add_user_role_relationship', 2),
(22, '2017_11_26_015000_create_user_roles_table', 2),
(23, '2018_03_11_000000_add_user_settings', 2),
(24, '2018_03_14_000000_add_details_to_data_types_table', 2),
(25, '2018_03_16_000000_make_settings_value_nullable', 2),
(26, '2020_06_22_171909_add_fecha_to_blogs_table', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(2, 'browse_bread', NULL, '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(3, 'browse_database', NULL, '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(4, 'browse_media', NULL, '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(5, 'browse_compass', NULL, '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(6, 'browse_menus', 'menus', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(7, 'read_menus', 'menus', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(8, 'edit_menus', 'menus', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(9, 'add_menus', 'menus', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(10, 'delete_menus', 'menus', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(11, 'browse_roles', 'roles', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(12, 'read_roles', 'roles', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(13, 'edit_roles', 'roles', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(14, 'add_roles', 'roles', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(15, 'delete_roles', 'roles', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(16, 'browse_users', 'users', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(17, 'read_users', 'users', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(18, 'edit_users', 'users', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(19, 'add_users', 'users', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(20, 'delete_users', 'users', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(21, 'browse_settings', 'settings', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(22, 'read_settings', 'settings', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(23, 'edit_settings', 'settings', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(24, 'add_settings', 'settings', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(25, 'delete_settings', 'settings', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(26, 'browse_hooks', NULL, '2020-06-22 10:02:21', '2020-06-22 10:02:21'),
(27, 'browse_cuadros', 'cuadros', '2020-06-22 10:10:01', '2020-06-22 10:10:01'),
(28, 'read_cuadros', 'cuadros', '2020-06-22 10:10:01', '2020-06-22 10:10:01'),
(29, 'edit_cuadros', 'cuadros', '2020-06-22 10:10:01', '2020-06-22 10:10:01'),
(30, 'add_cuadros', 'cuadros', '2020-06-22 10:10:01', '2020-06-22 10:10:01'),
(31, 'delete_cuadros', 'cuadros', '2020-06-22 10:10:01', '2020-06-22 10:10:01'),
(32, 'browse_blogs', 'blogs', '2020-06-22 10:11:32', '2020-06-22 10:11:32'),
(33, 'read_blogs', 'blogs', '2020-06-22 10:11:32', '2020-06-22 10:11:32'),
(34, 'edit_blogs', 'blogs', '2020-06-22 10:11:32', '2020-06-22 10:11:32'),
(35, 'add_blogs', 'blogs', '2020-06-22 10:11:32', '2020-06-22 10:11:32'),
(36, 'delete_blogs', 'blogs', '2020-06-22 10:11:32', '2020-06-22 10:11:32'),
(37, 'browse_contactos', 'contactos', '2020-06-22 10:12:18', '2020-06-22 10:12:18'),
(38, 'read_contactos', 'contactos', '2020-06-22 10:12:18', '2020-06-22 10:12:18'),
(39, 'edit_contactos', 'contactos', '2020-06-22 10:12:18', '2020-06-22 10:12:18'),
(40, 'add_contactos', 'contactos', '2020-06-22 10:12:18', '2020-06-22 10:12:18'),
(41, 'delete_contactos', 'contactos', '2020-06-22 10:12:18', '2020-06-22 10:12:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(32, 1),
(32, 2),
(33, 1),
(33, 2),
(34, 1),
(34, 2),
(35, 1),
(35, 2),
(36, 1),
(36, 2),
(37, 1),
(37, 2),
(38, 1),
(38, 2),
(39, 1),
(40, 1),
(41, 1),
(41, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-06-22 10:02:19', '2020-06-22 10:02:19'),
(2, 'user', 'Normal User', '2020-06-22 10:02:19', '2020-06-22 10:02:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\June2020\\6dwJumDxnW54jP4FrmXd.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'GaleríaIsaArte', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Bienvenido al administrador de GaleríaIsaArte', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\June2020\\FL0mfRhkoeDACBfeaLQv.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@galeriaisaarte.com', 'users/default.png', NULL, '$2y$10$WTMMIJidWSYrUTTCad.spOAxZtYMJ841UNoap3ZsD48PDxQxTIQGy', 'cYMox3OH57em644mVZvxjxH5I7flQZqURK51RxFfdV40wNJI2BNoAEDrWNJg', NULL, '2020-06-22 10:03:35', '2020-06-22 10:03:35'),
(2, 2, 'Administrador', 'user@galeriaisaarte.com', 'users/default.png', NULL, '$2y$10$SWlH5s1jIE11nNGYX8laNeKIHtsPt.LYdeVlLa6Om0tM2Xzk7m5R.', 'DCg6D3xwumTRZ9zlv6xT8i0dYHUaOIKSknrLA9qNOE1U09dInw6JqW791va7', NULL, '2020-06-22 10:16:29', '2020-06-22 10:16:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contactos_cuadro_id_foreign` (`cuadro_id`);

--
-- Indices de la tabla `cuadros`
--
ALTER TABLE `cuadros`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indices de la tabla `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indices de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indices de la tabla `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuadros`
--
ALTER TABLE `cuadros`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD CONSTRAINT `contactos_cuadro_id_foreign` FOREIGN KEY (`cuadro_id`) REFERENCES `cuadros` (`id`);

--
-- Filtros para la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
